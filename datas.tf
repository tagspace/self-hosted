data "aws_ami" "ubuntu" {
  most_recent = var.ubuntu_ami_build_number == "*" ? true : false
  filter {
    name   = "name"
    values = [
      "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-${var.ubuntu_ami_build_number}"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners      = ["099720109477"] # Canonical
}
