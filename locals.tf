locals {
  ses_region = var.ses_region == "" ? var.region : var.ses_region
  allowed_ips = concat([var.trevor_support_vpn_ip], var.allowed_ips)
}
