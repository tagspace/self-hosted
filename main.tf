### S3 setup

resource "aws_iam_user" "s3_app" {
  name          = "trevor_s3"
  force_destroy = true
}

resource "aws_iam_access_key" "s3_app" {
  user = aws_iam_user.s3_app.name
}

data "aws_iam_policy_document" "s3_policy" {
  statement {
    sid     = "AllowReadWriteBucket"
    actions = [
      "s3:ListBucket",
      "s3:GetBucketLocation",
      "s3:PutObject",
      "s3:PutObjectAcl",
      "s3:GetObject",
      "s3:GetObjectAcl",
    ]

    resources = [
      "arn:aws:s3:::${var.s3_bucket_name}",
      "arn:aws:s3:::${var.s3_bucket_name}/*",
    ]
  }
  statement {
    sid     = "AllowUseOfTheKey"
    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey",
    ]

    resources = [aws_kms_key.s3_key.arn]
  }
}

resource "aws_iam_policy" "s3_policy" {
  name   = "s3_policy"
  policy = data.aws_iam_policy_document.s3_policy.json
}

resource "aws_iam_user_policy_attachment" "s3_attach" {
  # checkov:skip=CKV_AWS_40:fixme: Review.
  user       = aws_iam_user.s3_app.name
  policy_arn = aws_iam_policy.s3_policy.arn
}

resource "aws_kms_key" "s3_key" {
  deletion_window_in_days = 14
  enable_key_rotation     = true
}

resource "aws_s3_bucket" "bucket" {
  # This bucket contains cached results from saved queries (e.g. for powering dashboards).
  # Each file only needs to be cached for 48 hours

  bucket        = var.s3_bucket_name
  acl           = "private"
  force_destroy = true

  versioning {
    enabled = true
  }

  tags = {
    Name = var.s3_bucket_name
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.s3_key.arn
        sse_algorithm     = "aws:kms"
      }
    }
  }

  lifecycle_rule {
    id      = "all"
    enabled = true
    expiration {
      days = 3 # must be at least 48 hours
    }
  }
}

resource "aws_s3_bucket_public_access_block" "public_access_block" {
  bucket                  = aws_s3_bucket.bucket.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "random_password" "trevor_encryption_key" {
  length = 32
}

### cloud-init preparation ###
data "template_cloudinit_config" "config" {
  gzip          = true
  base64_encode = true
  part {
    filename     = "init.cfg"
    content_type = "text/cloud-config"
    content      = templatefile("${path.module}/cloud-init/init.yml", {
      vm_name                      = "trevor"
      app_domain                   = var.domain
      aws_access_key_id            = var.trevor_client_key
      aws_secret_access_key        = var.trevor_client_secret
      rabbitmq_user                = local.rabbitmq_user
      rabbitmq_password            = local.rabbitmq_password
      postgres_host                = module.db.this_db_instance_endpoint
      postgres_password            = local.db_password
      postgres_user                = local.db_user
      postgres_db                  = var.db
      s3_access_key                = aws_iam_access_key.s3_app.id
      s3_secret_key                = aws_iam_access_key.s3_app.secret
      s3_bucket_name               = var.s3_bucket_name
      s3_writer_user_arn           = var.s3_writer_user_arn
      s3_writer_access_key         = var.s3_writer_access_key
      s3_writer_secret_key         = var.s3_writer_secret_key
      ses_region                   = local.ses_region
      ses_sender                   = var.ses_sender
      ses_smtp_username            = var.ses_smtp_username
      ses_smtp_password            = var.ses_smtp_password
      root_domain                  = "https://${var.domain}"
      trevor_encryption_key        = var.trevor_encryption_key == "" ? random_password.trevor_encryption_key.result : var.trevor_encryption_key
      registry_url                 = var.registry_url
      region                       = var.region
      support_oauth2_client_id     = var.support_oauth2_client_id
      support_oauth2_client_secret = var.support_oauth2_client_secret
      google_oauth2_client_id      = var.google_oauth2_client_id
      google_oauth2_client_secret  = var.google_oauth2_client_secret
      slack_oauth2_client_id       = var.slack_oauth2_client_id
      slack_oauth2_client_secret   = var.slack_oauth2_client_secret
      super_admins                 = join(",", var.super_admins)
      login_logo                   = var.login_logo
      logs_group                   = aws_cloudwatch_log_group.trevor_logs.name
      trevor_support_vpn_ip        = var.trevor_support_vpn_ip
      alarm_email                  = var.alarm_email
      allowed_ips                  = join(" ", local.allowed_ips)
      saml_auth_okta_enabled       = var.saml_auth_okta_enabled
      saml_auth_metadata_url       = var.saml_auth_metadata_url
      saml_auth_idp_issuer         = var.saml_auth_idp_issuer
      saml_keystore_location       = var.saml_keystore_location
      saml_keystore_password       = var.saml_keystore_password
      saml_keystore_alias          = var.saml_keystore_alias
      saml_auth_email_attr         = var.saml_auth_email_attr
      saml_auth_firstname_attr     = var.saml_auth_firstname_attr
      saml_auth_lastname_attr      = var.saml_auth_lastname_attr
    })
  }
}

### trevor EC2 ###

resource "random_password" "rabbitmq_password" {
  length           = 32
  override_special = "*()-_"
}

locals {
  rabbitmq_password = var.rabbitmq_password == "" ? random_password.rabbitmq_password.result : var.rabbitmq_password
}

resource "aws_ssm_parameter" "rabbitmq_password" {
  name  = "/trevor/rabbitmq_password"
  type  = "SecureString"
  value = local.rabbitmq_password
}

resource "random_string" "rabbitmq_user" {
  length           = 32
  override_special = "*()-_"
}

locals {
  rabbitmq_user = var.rabbitmq_user == "" ? random_string.rabbitmq_user.result : var.rabbitmq_user
}

resource "aws_ssm_parameter" "rabbitmq_user" {
  name  = "/trevor/rabbitmq_user"
  type  = "SecureString"
  value = local.rabbitmq_user
}

resource "aws_instance" "trevor" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.trevor_ec2_instance_type

  user_data_base64       = data.template_cloudinit_config.config.rendered

  subnet_id              = module.vpc.public_subnets[0]
  vpc_security_group_ids = [
    aws_security_group.trevor_instance_ingress.id,
    aws_security_group.trevor_instance_egress.id,
  ]
  ebs_optimized          = true
  monitoring             = true
  iam_instance_profile   = module.ssm.iam_profile_name

  tags = {
    Name    = "trevor"
    Service = "trevor"
  }
}

resource "aws_eip" "trevor" {
  instance   = aws_instance.trevor.id
  vpc        = true
  depends_on = [aws_instance.trevor]
}

resource "random_string" "ssm_log_bucket_suffix" {
  length  = 64
  special = false
  upper   = false
}

module "ssm" {
  source = "github.com/chrismazanec/terraform-aws-session-manager?ref=expanded-functionality"
  #  source                   = "github.com/bridgecrewio/terraform-aws-session-manager?ref=master"
  #  version                  = "0.2.0"

  #  bucket_name              = substr("session-logs-${random_string.ssm_log_bucket_suffix.result}", 0, 63)
  #  access_log_bucket_name   = var.access_logs_bucket_id
  enable_log_to_s3         = false
  enable_log_to_cloudwatch = true
}


resource "aws_cloudwatch_log_group" "trevor_logs" {
  name              = "trevor-logs"
  retention_in_days = var.log_retention_in_days
}

