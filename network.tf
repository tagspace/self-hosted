module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  name = "trevor"
  cidr = var.cidr
  azs             = var.azs
  public_subnets  = var.subnets

  enable_ipv6        = true
}
