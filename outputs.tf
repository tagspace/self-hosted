output "db_instance_endpoint" {
  description = "The connection endpoint"
  value       = module.db.this_db_instance_endpoint
}

output "trevor_instance_ip" {
  description = "The trevor instance ip"
  value       = aws_eip.trevor.public_ip
}
