### rds postgres ###

resource "random_password" "db_password" {
  length = 32
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

locals {
  db_password = var.db_password == "" ? random_password.db_password.result : var.db_password
}

resource "aws_ssm_parameter" "db_password" {
  name  = "/trevor/db_password"
  type  = "SecureString"
  value = local.db_password
}

resource "random_string" "db_user" {
  length  = 32
  special = false
}

locals {
  db_user = var.db_user == "" ? random_string.db_user.result : var.db_user
}

resource "aws_ssm_parameter" "db_user" {
  name  = "/trevor/db_user"
  type  = "SecureString"
  value = local.db_user
}

module "db" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 2.0"

  identifier                = "trevor"
  create_db_option_group    = false
  create_db_parameter_group = false

  # All available versions: https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_PostgreSQL.html#PostgreSQL.Concepts
  engine               = "postgres"
  engine_version       = "10.17"
  family               = "postgres10"
  # DB parameter group
  major_engine_version = "10"
  # DB option group
  instance_class       = "db.t3.medium"

  allocated_storage = 10

  name     = var.db
  username = local.db_user
  password = local.db_password
  port     = 5432

  multi_az               = false
  # subnet_ids             = var.rds_subnet_ids
  subnet_ids             = module.vpc.public_subnets
  vpc_security_group_ids = [aws_security_group.rds_ingress.id]

  maintenance_window = "Mon:00:00-Mon:03:00"
  apply_immediately  = true
  # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance
  backup_window      = "03:00-06:00"

  backup_retention_period = 7
  skip_final_snapshot     = true
  deletion_protection     = false

  parameters = [
    {
      name  = "autovacuum"
      value = 1
    },
    {
      name  = "client_encoding"
      value = "utf8"
    }
  ]
}

# resource "aws_route53_record" "rds" {
#   name    = "rds"
#   type    = "CNAME"
#   ttl     = 600
#   zone_id = var.internal_zone_id
#   records = [module.db.this_db_instance_address]
# }
