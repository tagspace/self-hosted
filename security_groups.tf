resource "aws_security_group" "trevor_instance_ingress" {
  #  name        = "trevor-ingress"
  description = "Allow inbound traffic"
  vpc_id      = module.vpc.vpc_id

  ##ingresses
  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = local.allowed_ips
  }
  ingress {
    description = "http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "https"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "trevor_instance_egress" {
  #  name        = "trevor-egress"
  description = "Allow outbound traffic"
  vpc_id      = module.vpc.vpc_id
  ##egresses
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "rds_ingress" {
  # checkov:skip=CKV2_AWS_5:This security group is attached to an RDS instance.
  #  name        = "rds"
  description = "Allow inbound traffic"
  vpc_id      = module.vpc.vpc_id
  ##ingresses
  ingress {
    description     = "postgresql"
    from_port       = 5432
    to_port         = 5432
    protocol        = "tcp"
    security_groups = [aws_security_group.trevor_instance_ingress.id]
  }
  lifecycle {
    create_before_destroy = true
  }
}
