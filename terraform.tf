terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.0"
    }
    random = {
      source = "hashicorp/random"
      version = ">= 3.0"
    }
    template = {
      source = "hashicorp/template"
      version = ">= 2.0"
    }
    http = {
      source = "hashicorp/http"
      version = ">= 2.0"
    }
  }

  backend "s3" {
    bucket  = "trevorio-terraform"
    key     = "tf-state" #this puts state into same bucket, but in separate
    region  = "eu-central-1"
    profile = "trevor"
  }
}
provider "aws" {
  region = var.region
  profile = "trevor"
}
