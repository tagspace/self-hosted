variable "region" {
  type = string
  default = "eu-central-1"
}

variable "trevor_ec2_instance_type" {
  default = "t3.small"
}

variable "db_user" {
  description = "The name of the initial user of the RDS database. If no value is specified, a random name is used."
  type        = string
  default     = ""
}

variable "db_password" {
  description = "The password of the initial user of the RDS database. If no value is specified, a random password is used."
  type        = string
  default     = ""
}

variable "db" {
  description = "The name of the initial database created in RDS."
  default     = "trevor"
}

variable "rabbitmq_user" {
  description = <<EOF
The password of the initial user of the RabbitMQ MQ. If no value is specified, a random name is used.
The value should be valid as a part of a URI (so special characters should be limited or the value should be urlencoded).
EOF
  default     = ""
}

variable "rabbitmq_password" {
  description = <<EOF
The password of the initial user of the RabbitMQ MQ. If no value is specified, a random name is used.
The value should be valid as a part of a URI (so special characters should be limited or the value should be urlencoded).
EOF
  default     = ""
}

variable "support_oauth2_client_id" {
  type = string
  default = "missing"
}

variable "support_oauth2_client_secret" {
  type = string
  default = "missing"
}

variable "google_oauth2_client_id" {
  type = string
  default = "missing"
}

variable "google_oauth2_client_secret" {
  type = string
  default = "missing"
}

variable "slack_oauth2_client_id" {
  type = string
  default = "missing"
}

variable "slack_oauth2_client_secret" {
  type = string
  default = "missing"
}

variable "super_admins" {
  type = list(string)
  default = []
}

variable "login_logo" {
  type = string
  default = "/img/rocket-tr.png"
}

variable "s3_bucket_name" {
  type = string
  default = "trevor-bucket"
}

variable "s3_writer_user_arn" {
  type = string
  default = "missing"
}

variable "s3_writer_access_key" {
  type = string
  default = "missing"
}

variable "s3_writer_secret_key" {
  type = string
  default = "missing"
}

variable "registry_url" {
  description = "The url of the Docker registry where the Trevor container is stored."
  default     = "049968283965.dkr.ecr.eu-central-1.amazonaws.com"
  type        = string
}

variable "trevor_client_key" {
  type = string
  default = "missing"
}

variable "trevor_client_secret" {
  type = string
  default = "missing"
}

variable "ses_region" {
  default = ""
  type    = string
}

variable "ses_sender" {
  type = string
  default = "team@trevor.io"
}

variable "ses_smtp_username" {
  type = string
  default = "missing"
}

variable "ses_smtp_password" {
  type = string
  default = "missing"
}

variable "trevor_encryption_key" {
  default = ""
}

variable "alarm_email" {
  type = string
  default = "support@trevor.io"
}

variable "subnets" {
  default = ["172.31.100.0/24","172.31.101.0/24"]
}

variable "azs" {
  default = ["eu-central-1a","eu-central-1b"]
}

variable "cidr" {
  default = "172.31.100.0/22"
}

variable "trevor_support_vpn_ip" {
  default = "37.120.196.235/32"
}

variable "allowed_ips" {
  default = ["0.0.0.0/0"] 
}

variable "access_logs_bucket_id" {
  type = string
  default = "value"
}

variable "ubuntu_ami_build_number" {
  description = "The unique identifier/build number of the Canonical Ubuntu AMI. If no value is specified, the latest AMI is used."
  default     = "*"
  type        = string
}

variable "external_zone_id" {
  type = string
  default = "eu-central-1"
}

variable "log_retention_in_days" {
  type    = number
  default = 1827
}

variable "domain" {
  type    = string
  default = "127.0.0.1:8080"
}

# SAML configuration variables

# If this value is true then the only enabled SSO login provider will be Okta
variable "saml_auth_okta_enabled" {
  type = string
  default = "false"
}

# For Okta: should match URL in Okta > Admin > App > Sign On > Identity Provider Metadata
variable "saml_auth_metadata_url" {
  type = string
  default = "missing"
}

# For Okta: should match value in Okta > Admin > App > Sign On > Setup Instructions > Identity Provider Issuer
variable "saml_auth_idp_issuer" {
  type = string
  default = "missing"
}

# Absolute path of where the keystore file is stored
variable "saml_keystore_location" {
  type = string
  default = "missing"
}

# Password to open the keystore
variable "saml_keystore_password" {
  type = string
  default = "missing"
}

# Alias to identify the private key inside the keystore
variable "saml_keystore_alias" {
  type = string
  default = "missing"
}

# Expected name of the email attribute in the SAML assertion
variable "saml_auth_email_attr" {
  type = string
  default = "email"
}

# Expected name of the first name attribute in the SAML assertion
variable "saml_auth_firstname_attr" {
  type = string
  default = "firstName"
}

# Expected name of the last name attribute in the SAML assertion
variable "saml_auth_lastname_attr" {
  type = string
  default = "lastName"
}
